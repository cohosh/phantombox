#!/bin/bash

# Find iface name for default network
default_iface=$(virsh --connect qemu:///system net-info --network default | sed -En -e 's/.*Bridge:(\s)+/\1/p' | xargs)
echo "Default network iface is $default_iface"

# Detect default libvirt network subnet
default_inet=$(ip -f inet addr show $default_iface | sed -En -e 's/.*inet ([0-9.]+).*/\1/p')
echo "Using default libvirt inet addr $default_inet"
IFS='.' read -ra ip <<< "$default_inet"
ip_prefix="${ip[0]}.${ip[1]}.${ip[2]}"

# update IP addresses of hosts on default network
virsh --connect qemu:///system net-update default add ip-dhcp-host \
          "<host mac='52:54:00:00:01:02' \
           name='station' ip='$ip_prefix.2' />" \
           --live --config 2> /dev/null
virsh --connect qemu:///system net-update default add ip-dhcp-host \
          "<host mac='52:54:00:00:01:03' \
           name='pt' ip='$ip_prefix.3' />" \
           --live --config 2> /dev/null
virsh --connect qemu:///system net-update default add ip-dhcp-host \
          "<host mac='52:54:00:00:01:04' \
           name='tap' ip='$ip_prefix.4' />" \
           --live --config 2> /dev/null

# create the internal "tap" network
virsh --connect qemu:///system net-create network.xml

if [ $(lsb_release -is) = "Debian" ]; then
    # Allow forwarded packets from internal tap network
    sudo iptables -I LIBVIRT_FWO 1 -s 192.168.2.0/24 -i $default_iface -j ACCEPT
    sudo iptables -I LIBVIRT_FWI 1 -d 192.168.2.0/24 -o $default_iface -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    # Ensure packets from internal tap network are properly NAT'd
    sudo iptables -t nat -A LIBVIRT_PRT -s 192.168.2.0/24 ! -d $ip_prefix.0/24 -j MASQUERADE
elif [ $(lsb_release -is) = "Fedora" ]; then
    sudo nft insert rule libvirt_network guest_output ip saddr 192.168.2.0/24 iif "$default_iface" counter accept
    sudo nft insert rule libvirt_network guest_nat ip saddr 192.168.2.0/24 ip daddr != $ip_prefix.0/24 counter masquerade
    sudo nft insert rule libvirt_network guest_input oif "$default_iface" ip daddr 192.168.2.0/24 ct state {related, established} counter accept
else
    echo "Untested OS. Try applying above iptables or nftables rules manually and submit a MR to us :)"
fi

# Route all inbound packets to the internal network through the tap machine
sudo ip route add 192.168.2.4 dev $default_iface
sudo ip route change 192.168.2.0/24 via 192.168.2.4 dev $default_iface

